# DigitalOcean Scripts

A collection of scripts to use the Digital Ocean API

## Getting started

```
git clone https://gitlab.com/geekynick/digitalocean-scripts.git
pip install -r requirements.txt
```

Add your DigitalOcean API Key into a file called `.creds`, or just let the script prompt you for it.

All written in python3.6.1.

## Scripts included

### Destroy all droplets

Destroys all droplets in the account. WARNING: There is no confirmation, it just does it.
Usage: `python destroyalldroplets.py`

### Create some iperf droplets

Creates a specified number of iperf droplets, running iperf as a server ready for connections.
Usage: `python createiperfdroplets.py 2`, where 2 is the required number of iperf droplets.
The IP's will be output when the script completes.

## Author
* **Nick Shaw** - [Geeky Nick](https://www.geekynick.co.uk)

## Affiliation

The author is in no way affiliated with DigitalOcean.