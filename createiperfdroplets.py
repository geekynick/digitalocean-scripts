#!/usr/bin/env python3
"""
script to create a specific number of droplets preconfigured to run
iperf3 server.

Command line args: python3 createiperfdroplets.py <num>
<num>: The number of droplets to create.

Nick Shaw
www.alwaysnetworks.co.uk
"""

from sys import argv
import digitalocean
import creds


def createiperfdroplet(dropletname):
    """create a single iperf droplet"""
    apikey = creds.getapikey()
    manager = digitalocean.Manager(token=apikey)
    keys = manager.get_all_sshkeys()

    user_data = '#!/bin/bash\n' \
                'apt-get update -y\n' \
                'apt-get install -y iperf3\n' \
                'iperf3 -s &'

    droplet = digitalocean.Droplet(
        token=apikey,
        name=dropletname,
        region='lon1',
        image='ubuntu-16-04-x64',
        size_slug='512mb',
        ssh_keys=keys,
        user_data=user_data
    )

    droplet.create()
    actions = droplet.get_actions()
    completed = False
    while not completed:
        for action in actions:
            action.load()
            # Once it shows complete, droplet is up and running
            if action.status == 'completed':
                completed = True
    droplet.load()
    return droplet


if __name__ == '__main__':
    try:
        droplets = []
        qty = int(argv[1])
        for index in range(0, qty):
            dropname = 'iperf-{}'.format(index+1)
            print('Creating {}...'.format(dropname))
            droplet = createiperfdroplet(dropname)
            droplets.append(droplet)
        for droplet in droplets:
            print('{}: {}'.format(droplet.name, droplet.ip_address))
    except IndexError:
        print("You need to pass 1 command line parameter for the quantity")
    except ValueError:
        print('You need to pass an integer')
