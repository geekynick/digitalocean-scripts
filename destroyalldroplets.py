#!/usr/bin/env python3
"""
A script to destroy all digital ocean droplets

"""

import digitalocean
import creds


def destroyalldroplets():
    """destroy all digital ocean droplets in an account"""
    apikey = creds.getapikey()
    manager = digitalocean.Manager(token=apikey)
    alldroplets = manager.get_all_droplets()
    for droplet in alldroplets:
        print('Found droplet: {}. Destroying...'.format(droplet.name))
        droplet.destroy()


if __name__ == '__main__':
    destroyalldroplets()
