#!/usr/bin/env python3
"""Asks for an API key, or loads it from .creds"""


def getapikey():
    """Gets the API key from .creds, or asks the user"""
    try:
        with open('.creds', 'r') as credfile:
            return credfile.read().strip()
    except FileNotFoundError:
        return input('Enter your API Key: ')
